# Generated by Django 3.2 on 2021-08-26 11:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0004_auto_20210826_0918'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookinginfo',
            name='listing',
        ),
    ]
