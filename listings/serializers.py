from rest_framework import serializers
from .models import BookingInfo, Reservation
class BookingInfoSerialzer(serializers.ModelSerializer):
    class Meta:
        model = BookingInfo
        fields = "__all__"

class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = "__all__"