import datetime

from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.filters import OrderingFilter
from .serializers import ReservationSerializer
from .models import Reservation, BookingInfo
from django.db.models import Q

class BookingViewSet(ListAPIView):
    authentication_classes = ()
    permission_classes = ()
    filter_backends = [OrderingFilter]
    ordering_fields = ['price']
    ordering = ['price', ]

    def get(self, request):
        if not 'max_price' in self.request.GET:
            return Response({'message': 'max_price is required'})
        if not 'check_in' in self.request.GET:
            return Response({'message': 'check_in is required'})
        if not 'check_out' in self.request.GET:
            return Response({'message': 'check_out is required'})
        max_price = self.request.GET.get('max_price', None)
        check_in = datetime.datetime.strptime(str(self.request.GET.get('check_in', None)), '%Y-%m-%d').date()
        check_out = datetime.datetime.strptime(str(self.request.GET.get('check_out', None)), '%Y-%m-%d').date()
        reservations = Reservation.objects.filter(Q(Q(check_in__lte=check_in) & Q(check_out__gte=check_in)) | Q(Q(check_in__lte=check_out) & Q(check_out__gte=check_out)))
        bookings = [res.booking_id for res in reservations]
        print(bookings)
        available_bookings = BookingInfo.objects.exclude(id__in=bookings).filter(price__lte=max_price).order_by('price')
        data = []
        for avail in available_bookings:
            dict = {}
            dict["listing_type"] = avail.hotel_room_type.hotel.listing_type
            dict["title"] = avail.hotel_room_type.hotel.title
            dict["country"] = avail.hotel_room_type.hotel.country
            dict["city"] = avail.hotel_room_type.hotel.city
            dict["price"] = str(avail.price)
            data.append(dict)
        return Response({"items": data})

class ReservationView(ListAPIView):
    authentication_classes = ()
    permission_classes = ()
    def post(self, request):
        data = request.data
        serializer = ReservationSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        return Response({'token': data})

